This module extend theme permission.
Allow edit theme for specific role.

===================
How to use ?

    1) Activate module
    2) Go to admin/people/permissions and select theme permission for specific
    role
