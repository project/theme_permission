<?php

declare(strict_types=1);

namespace Drupal\Tests\theme_permission\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Administration theme access check.
 *
 * @group theme_permission
 */
abstract class ThemePermissionTestBase extends BrowserTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'user',
    'system',
    'theme_permission',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable9';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    \Drupal::service('theme_installer')->install(['stable9', 'olivero']);

    $settings = [
      'theme' => 'stable9',
      'region' => 'header',
    ];
    // Place a block.
    $this->drupalPlaceBlock('local_tasks_block', $settings);
  }

  /**
   * Drupal User login.
   *
   * @param array|null $permissions
   *   User permission.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function userLogin(array $permissions = NULL): void {

    $permissions = $permissions ?? [];
    $userPermission = array_merge(
      $permissions,
      [
        'administer themes',
        'administer blocks',
      ]
    );

    $user = $this->drupalCreateUser($userPermission);
    if ($user) {
      $this->drupalLogin($user);
    }
  }

}
