<?php

declare(strict_types=1);

namespace Drupal\Tests\theme_permission\Functional;

/**
 * Administration theme access check.
 *
 * @group theme_permission
 */
class AccessBlockBrowserTest extends ThemePermissionTestBase {

  /**
   * Check if user access to stable9 blocks configuration.
   */
  public function testIfAccessThemeStable9(): void {
    $this->userLogin(['administer themes stable9']);
    $this->drupalGet('/admin/structure/block/list/stable9');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Check if user don't access to stable9 blocks configuration.
   */
  public function testIfAccessDeniedThemeStable9(): void {
    $this->userLogin();
    $this->drupalGet("/admin/structure/block/list/stable9");
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check if block list contain olivero url.
   */
  public function testIfShowOlivero(): void {
    $this->userLogin(
      [
        'administer themes stable9',
        'administer themes olivero',
      ]
    );
    $this->drupalGet('/admin/structure/block');
    $this->assertNotEmpty($this->getSession()->getPage()->find('xpath', '//a[contains(@href, "/admin/structure/block/list/olivero")]'));
  }

  /**
   * Check if block list don't contain olivero url.
   */
  public function testIfNotShowOlivero(): void {
    $this->userLogin();
    $this->drupalGet("/admin/structure/block");
    $this->assertEmpty($this->getSession()->getPage()->find('xpath', '//a[contains(@href, "/admin/structure/block/list/olivero")]'));
  }

}
